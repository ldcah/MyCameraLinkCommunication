﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace MyCameraLinkCommunication
{
    public partial class Form1 : Form
    {
        // Handle to the serial port
        IntPtr serialRef;

        // Index of the serial port
        UInt32 serialIndex;

        // Number of serial ports
        UInt32 numPorts;
       
        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {           


        }

        private void Form1_Load(object sender, EventArgs e)
        {

            string ManufacturerName="", PortID=""; uint Version=0;
          
            //获取端口数量
            MyClAllSerial.GetNumPorts(out numPorts);

            // Retrieve the identifier of each port 
            for (uint i = 0; i < numPorts; i++)
            {
                //获取端口信息
                MyClAllSerial.GetPortInfo(i, ref ManufacturerName, ref PortID, ref Version);
                comboBox1.Items.Add(PortID);               
            }
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Open the serial port
                MyClAllSerial.SerialInit(serialIndex, out serialRef);

                // Update the GUI
                textBox2.Text = "";
                textBox1.Text = "";
                closeButton.Enabled = true;
                openButton.Enabled = false;
                sendButton.Enabled = true;
                readButton.Enabled = true;
                textBox1.Enabled = true;
                comboBox1.Enabled = false;
            }
            catch (clSerialException error)
            {
                textBox2.Text = error.ToString();
            }
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Close the serial port
                MyClAllSerial.SerialClose(serialRef);

                // Update the GUI
                textBox2.Text = "";
                textBox1.Text = "";
                textBox1.Enabled = false;
                comboBox1.Enabled = true;
                sendButton.Enabled = false;
                readButton.Enabled = false;
                closeButton.Enabled = false;
                openButton.Enabled = true;
            }
            catch (clSerialException error)
            {
                textBox2.Text = error.ToString();
            }
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Write the command to the port
                textBox1.Text += Convert.ToChar(13);
                UInt32 numBytes = (UInt32)textBox1.Text.Length;
                MyClAllSerial.SerialWrite(serialRef, textBox1.Text, out numBytes, 5000);
                // Empty the text boxes
                //textBox1.Text = "";
                textBox2.Text = "";
            }
            catch (clSerialException error)
            {
                textBox2.Text = error.ToString();
                return;
            }


            System.Threading.Thread.Sleep(100);

            
            //读取反馈信息
            while (true)
            {
                UInt32 numBytes;
                MyClAllSerial.GetNumBytesAvail(serialRef, out numBytes);

                if (numBytes == 0)
                    break;
                else
                {
                    // Retrieve the data in the read buffer
                    IntPtr receivedData = Marshal.AllocHGlobal((int)numBytes + 1);
                    MyClAllSerial.SerialRead(serialRef, receivedData, out numBytes, 5000);
                    String data = Marshal.PtrToStringAnsi(receivedData, (int)numBytes - 1);
                    Marshal.FreeHGlobal(receivedData);
                    textBox2.Text += data;
                }
                System.Threading.Thread.Sleep(20);
            }

        }

        private void readButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Retrieve the number of bytes in the read buffer
                UInt32 numBytes;
                MyClAllSerial.GetNumBytesAvail(serialRef, out numBytes);

                if (numBytes == 0)
                    textBox2.Text = "<NO DATA>";
                else
                {
                    // Retrieve the data in the read buffer
                    IntPtr receivedData = Marshal.AllocHGlobal((int)numBytes + 1);
                    MyClAllSerial.SerialRead(serialRef, receivedData, out numBytes, 5000);
                    String data = Marshal.PtrToStringAnsi(receivedData, (int)numBytes - 1);
                    Marshal.FreeHGlobal(receivedData);
                    textBox2.Text = data;
                }
            }
            catch (clSerialException error)
            {
                textBox2.Text = error.ToString();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            serialIndex = (UInt32)comboBox1.SelectedIndex;

            // Update the GUI
            openButton.Enabled = true;
        }

        private void btSet_Click(object sender, EventArgs e)
        {
            try
            {
                // Retrieve the number of bytes in the read buffer
                UInt32 baudRate=Convert.ToUInt32(txtBaudRate.Text);
                MyClAllSerial.SetBaudRate(serialRef, baudRate);
             
            }
            catch (clSerialException error)
            {
                textBox2.Text = error.ToString();
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                // Retrieve the number of bytes in the read buffer
                UInt32 baudRate;
                MyClAllSerial.GetSupportedBaudRates(serialRef,out baudRate);

                txtGetBaudRate.Text = baudRate.ToString();

            }
            catch (clSerialException error)
            {
                textBox2.Text = error.ToString();
            }
        }
    }
}
