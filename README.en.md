# MyCameraLinkCommunication

#### Description
利用 MVTec\HALCON-12.0\genicam\bin\Win64_x64\CLAllSerial_MD_VC80_v2_3.dll 文件访问 vieworks相机

参考 Euresys 例子实现 cameraLink 访问相机  改变相机  direction  ssd 0   ssd 1  
CameraLink20v1.13  说明文档 https://wenku.baidu.com/view/cffb3cfe9e31433239689397.html

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)